var searchData=
[
  ['insert',['insert',['../classhashing_1_1_vich.html#aaad1f2085bb189b56067d4c8cf3b9e34',1,'hashing::Vich::insert(key_type const &amp;key, mapped_type const &amp;v) noexcept -&gt; Result'],['../classhashing_1_1_vich.html#ae497c57a9c1cf1060f11ca5096afa997',1,'hashing::Vich::insert(key_type const &amp;key, mapped_type &amp;&amp;v) noexcept -&gt; Result'],['../classhashing_1_1_vich.html#a6af9c72cbb3d7b1e31ec2f6ad6180fd1',1,'hashing::Vich::insert(key_type &amp;&amp;key, mapped_type &amp;&amp;v) noexcept -&gt; Result']]],
  ['insert_5for_5fassign',['insert_or_assign',['../classhashing_1_1_vich.html#a88f9f9a6cd36eab8f88c8d7532607f63',1,'hashing::Vich::insert_or_assign(key_type const &amp;key, mapped_type &amp;&amp;v) noexcept -&gt; Result'],['../classhashing_1_1_vich.html#a15a066b9809f8fb9dd77c357952a0168',1,'hashing::Vich::insert_or_assign(key_type &amp;&amp;key, mapped_type &amp;&amp;v) noexcept -&gt; Result']]]
];
