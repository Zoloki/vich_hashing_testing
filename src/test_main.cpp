
#include <iostream>
#include "catch.hpp"
#include "vich_hashing.hpp"
#include <map>
#include "sole.hpp"
#include <unordered_set>
#include <chrono>
#include "flat_hash_map.hpp"

struct TestHasher
{
  int operator()(std::string const& str) const noexcept {
    if (str == "Francis") { return 0; }
    if (str == "Don")     { return 2; }
    if (str == "Leo")     { return 0; }
    if (str == "Mike")    { return 2; }
    if (str == "Jeff")    { return 0; }
    if (str == "Dan")     { return 8; }
    if (str == "Gary")    { return 7; }
    if (str == "Wen")     { return 8; }
    if (str == "Sharon")  { return 6; }
    if (str == "Dieter")  { return 1; }
    return -1;
  }
};

auto k1 = std::string{"Francis"};
auto k2 = std::string{"Don"};
auto k3 = std::string{"Leo"};
auto k4 = std::string{"Mike"};
auto k5 = std::string{"Jeff"};
auto k6 = std::string{"Dan"};
auto k7 = std::string{"Gary"};
auto k8 = std::string{"Wen"};
auto k9 = std::string{"Sharon"};


using VichTestType = hashing::Vich<std::string, int, TestHasher>;

TEST_CASE("testing construction", "vich_hashing") {

  SECTION("specifying the bucket count only") {

    VichTestType table{9};

    REQUIRE_THAT(table.load_factor(), Catch::WithinULP(0.0, 4));
    REQUIRE_THAT(table.address_factor(), Catch::WithinULP(0.86, 4));
    REQUIRE(table.empty());
    REQUIRE(table.size() == 0);
    REQUIRE(table.capacity() == 11);

  }

  SECTION("specifying the bucket count and the cellar count") {

    VichTestType table{9l, 2l};

    REQUIRE_THAT(table.load_factor(), Catch::WithinULP(0.0, 4));
    REQUIRE_THAT(table.address_factor(), Catch::WithinULP(9.0 / 11.0, 4));
    REQUIRE(table.empty());
    REQUIRE(table.size() == 0);
    REQUIRE(table.capacity() == 11);

  }

  SECTION("specifying the bucket count and the address factor") {

    VichTestType table{9, VichTestType::cellar_count(9, 0.75)};

    REQUIRE_THAT(table.load_factor(), Catch::WithinULP(0.0, 4));
    REQUIRE_THAT(table.address_factor(), Catch::WithinULP(0.75, 4));
    REQUIRE(table.empty());
    REQUIRE(table.size() == 0);
    REQUIRE(table.capacity() == 12);

  }
}

TEST_CASE("testing item insertion", "vich_hashing") {

  VichTestType table{9};

  std::vector<std::pair<std::string, int> > data{

    {"Francis", 1}, {"Don", 2}, {"Leo", 3},
    {"Mike", 4}, {"Jeff", 5}, {"Dan", 6},
    {"Gary", 7}, {"Wen", 8}, {"Sharon", 9}

  };

  SECTION("insert(key_type const& key, mapped_type&& v)") {

    for (auto& [key, value] : data) {
      auto r = table.insert(key, std::move(value));
      REQUIRE(r == VichTestType::Result::success);
    }

  }

  SECTION("insert(key_type&& key, mapped_type&& v") {

    for (auto& [key, value] : data) {
      auto r = table.insert(std::move(key), std::move(value));
      REQUIRE(r == VichTestType::Result::success);
    }

  }

  SECTION("insert_or_assign(key_type const& key, mapped_type&& v") {

    for (auto& [key, value] : data) {
      auto r = table.insert(key, std::move(value));
      REQUIRE(r == VichTestType::Result::success);
    }

  }

  SECTION("insert_or_assign(key_type&& key, mapped_type&& v") {

    for (auto& [key, value] : data) {
      auto r = table.insert(std::move(key), std::move(value));
      REQUIRE(r == VichTestType::Result::success);
    }

  }
}

TEST_CASE("access", "vich_hashing") {

  std::vector<std::tuple<std::string, int> > data{

    {"Francis", 1}, {"Don", 2}, {"Leo", 3},
    {"Mike", 4}, {"Jeff", 5}, {"Dan", 6},
    {"Gary", 7}, {"Wen", 8}, {"Sharon", 9}

  };

  VichTestType table{9};

  for (auto& [key, value] : data) {
    auto r = table.insert(key, value);
    REQUIRE(r == VichTestType::Result::success);
  }

  REQUIRE_FALSE(table.empty());
  REQUIRE(table.capacity() == 11);
  REQUIRE(table.size() == 9);

  SECTION("has") {

    for (auto& [key, value] : data) {
      REQUIRE(table.has(key));
      (void)value;
    }

  }

  SECTION("constant access") {

    SECTION("at const") {

      auto const& t = table;

      // access existing items
      for (auto& [key, value] : data) {

        int v;
        REQUIRE_NOTHROW(v = t.at(key));
        REQUIRE(v == value);

      }

      // access non existing items, expect exceptions of type std::out_of_range
      REQUIRE_THROWS_AS(t.at("Dieter"), std::out_of_range);
      REQUIRE_THROWS_AS(t.at(""), std::out_of_range);

    }

    SECTION("find const") {

      auto& t = table;

      // access existing items
      for (auto& [key, value] : data) {
        auto vPtr = t.find(key);
        REQUIRE(vPtr != nullptr);
        REQUIRE(*vPtr == value);
      }

      // access non existing items, expect exceptions of type std::out_of_range
      {
        auto vPtr = t.find("Dieter");
        REQUIRE(vPtr == nullptr);
      }
      {
        auto vPtr = t.find("");
        REQUIRE(vPtr == nullptr);
      }
    }
  }

  SECTION("non constant access") {

    SECTION("at") {

      auto& t = table;

      // access existing items
      for (auto& [key, value] : data) {

        int v;
        REQUIRE_NOTHROW(v = t.at(key));
        REQUIRE(v == value);

      }

      // access non existing items, expect exceptions of type std::out_of_range
      REQUIRE_THROWS_AS(t.at("Dieter"), std::out_of_range);
      REQUIRE_THROWS_AS(t.at(""), std::out_of_range);

      // access existing items in order to modify their values
      {
        int new_value = 42;
        auto& v = t.at("Francis");
        REQUIRE_FALSE(v == new_value);
        v = new_value;

        // at this point "Francis" should have the new value
        auto vPtr = t.find("Francis");
        REQUIRE(vPtr != nullptr);
        REQUIRE(*vPtr == new_value);
      }

    }

    SECTION("find") {

      auto& t = table;

      // access existing items
      for (auto& [key, value] : data) {
        auto vPtr = t.find(key);
        REQUIRE(vPtr != nullptr);
        REQUIRE(*vPtr == value);
      }

      // access non existing items, expect exceptions of type std::out_of_range
      {
        auto vPtr = t.find("Dieter");
        REQUIRE(vPtr == nullptr);
      }
      {
        auto vPtr = t.find("");
        REQUIRE(vPtr == nullptr);
      }

      // access existing items in order to modify their values
      {
        int new_value = 42;
        auto vPtr = t.find("Francis");
        REQUIRE(vPtr != nullptr);
        *vPtr = new_value;

        // at this point "Francis" should have the new value
        auto v = t.at("Francis");
        REQUIRE(v == new_value);
      }
    }
  }
}

TEST_CASE("erase", "vich_hashing") {

  std::vector<std::tuple<std::string, int, int>> data{

    {"Francis", 1, 2 }, {"Don", 2, 4 }, {"Leo", 3, 12 },
    {"Mike", 4, 11 }, {"Jeff", 5, 10 }, {"Dan", 6, 9 },
    {"Gary", 7, 8 }, {"Wen", 8, 7 }, {"Sharon", 9, 6 }

  };

  VichTestType table{9};

  for (auto& [key, value, index] : data) {
    auto r = table.insert(key, value);
    REQUIRE(r == VichTestType::Result::success);
    (void)value;
    (void)index;
  }

  REQUIRE_FALSE(table.empty());
  REQUIRE(table.capacity() == 11);
  REQUIRE(table.size() == 9);

  SECTION("has") {

    for (auto& [key, value, index] : data) {
      REQUIRE(table.has(key));
      (void)value;
      (void)index;
    }

  }

  SECTION("erase an item which is the sole item of its bucket") {

    // Insert an item which will be alone in its bucket so we can erase it.
    table.insert("Dieter", 23);
    REQUIRE(table.has("Dieter"));

    auto r = table.erase("Dieter");
    REQUIRE(r == VichTestType::Result::success);
    REQUIRE_FALSE(table.has("Dieter"));

    REQUIRE(table.find("Francis") != nullptr);
    REQUIRE(table.find("Don") != nullptr);
    REQUIRE(table.find("Leo") != nullptr);
    REQUIRE(table.find("Mike") != nullptr);
    REQUIRE(table.find("Jeff") != nullptr);
    REQUIRE(table.find("Dan") != nullptr);
    REQUIRE(table.find("Gary") != nullptr);
    REQUIRE(table.find("Wen") != nullptr);
    REQUIRE(table.find("Sharon") != nullptr);

  }

  SECTION("erase an item which is the end of a chain") {

    auto r = table.erase("Sharon");
    REQUIRE(r == VichTestType::Result::success);
    REQUIRE_FALSE(table.has("Dieter"));

    REQUIRE(table.find("Francis") != nullptr);
    REQUIRE(table.find("Don") != nullptr);
    REQUIRE(table.find("Leo") != nullptr);
    REQUIRE(table.find("Mike") != nullptr);
    REQUIRE(table.find("Jeff") != nullptr);
    REQUIRE(table.find("Dan") != nullptr);
    REQUIRE(table.find("Gary") != nullptr);
    REQUIRE(table.find("Wen") != nullptr);

  }

  SECTION("erase Don") {

    auto r = table.erase("Don");
    REQUIRE(r == VichTestType::Result::success);
    REQUIRE_FALSE(table.has("Don"));

    REQUIRE(table.find("Francis") != nullptr);
    REQUIRE(table.find("Mike") != nullptr);
    REQUIRE(table.find("Wen") != nullptr);
    REQUIRE(table.find("Sharon") != nullptr);
    REQUIRE(table.find("Gary") != nullptr);
    REQUIRE(table.find("Dan") != nullptr);
    REQUIRE(table.find("Jeff") != nullptr);
    REQUIRE(table.find("Leo") != nullptr);

  }

  SECTION("erase Mike") {

    auto r = table.erase("Mike");
    REQUIRE(r == VichTestType::Result::success);
    REQUIRE_FALSE(table.has("Mike"));

    REQUIRE(table.find("Francis") != nullptr);
    REQUIRE(table.find("Don") != nullptr);
    REQUIRE(table.find("Leo") != nullptr);
    REQUIRE(table.find("Jeff") != nullptr);
    REQUIRE(table.find("Dan") != nullptr);
    REQUIRE(table.find("Gary") != nullptr);
    REQUIRE(table.find("Sharon") != nullptr);
    REQUIRE(table.find("Wen") != nullptr);

  }
}

TEST_CASE("random insert, access and erase", "vich_hashing") {

  using key_type = int;
  using mapped_type = int;
  using namespace std::chrono;

  int count{1000000};

  // Generate the operations
  std::mt19937 gen{};
  std::uniform_int_distribution<> operation(1, 3);
  std::uniform_int_distribution<> index(0, count - 1);

  std::vector<std::pair<int, int> > ops;
  ops.reserve(count * 10);
  for (auto i = 0; i < (count * 10); ++i) {
    ops.emplace_back(operation(gen), index(gen));
  }

  // Into this unordered set we store all items which were inserted into
  // our hash table to verify the response of the insert, find and
  // erase methods.
  std::unordered_set<key_type> inserted;

  // Create a number of keys, half of them are used to populate the
  // the table initially, the other half is a pool from which we
  // insert later.
  std::vector<std::pair<key_type, mapped_type> > data;
  data.reserve(count);

  for (auto i = 0; i < count; ++i) {
    //data.emplace_back(sole::uuid4().str(), i + 1);
    data.emplace_back(i, i + 1);
  }

  SECTION("vich_hashing") {

    hashing::Vich<key_type, mapped_type> table{count};

    auto t1 = high_resolution_clock::now();

    for (auto i = 0; i < (count >> 1); ++i) {
      table.insert(data[i].first, data[i].second);
      inserted.insert(data[i].first);
    }

    auto t2 = high_resolution_clock::now();

    for (auto [op, idx] : ops) {

      switch(op) {

        case 1: // insert
        {
          auto result = table.insert(data[idx].first, data[idx].second);
          [[maybe_unused]] auto [it, flag] = inserted.insert(data[idx].first);
          (void)it;
          (void)flag;
          (void)result;
          break;
        }

        case 2: // retrieve
        {
          auto vPtr = table.find(data[idx].first);
          (void)vPtr;
          break;
        }

        case 3: // erase
        {
          auto result = table.erase(data[idx].first);
          (void)result;
          break;
        }

      }
    }

    auto t3 = high_resolution_clock::now();

    std::cout << "vich_hashing: " << duration_cast<milliseconds>(t2 - t1).count()
              << ", " << duration_cast<milliseconds>(t3 - t2).count() << std::endl;
  }

  SECTION("unordered_map") {

    using table_t = std::unordered_map<key_type, mapped_type>;
    table_t table{static_cast<table_t::size_type>(count)};

    auto t1 = high_resolution_clock::now();

    for (auto i = 0; i < (count >> 1); ++i) {
      table.emplace(data[i].first, data[i].second);
      inserted.insert(data[i].first);
    }

    auto t2 = high_resolution_clock::now();

    for (auto [op, idx] : ops) {

      switch(op) {

        case 1: // insert
        {
          auto result = table.emplace(data[idx].first, data[idx].second);
          [[maybe_unused]] auto it = inserted.insert(data[idx].first);
          (void)it;
          (void)result;
          break;
        }

        case 2: // retrieve
        {
          auto vPtr = table.find(data[idx].first);
          (void)vPtr;
          break;
        }

        case 3: // erase
        {
          auto result = table.erase(data[idx].first);
          (void)result;
          break;
        }

      }
    }

    auto t3 = high_resolution_clock::now();

    std::cout << "unordered_map: " << duration_cast<milliseconds>(t2 - t1).count()
              << ", " << duration_cast<milliseconds>(t3 - t2).count() << std::endl;
  }

  SECTION("flat_hash_map") {

    using table_t = ska::flat_hash_map<key_type, mapped_type>;
    table_t table{static_cast<table_t::size_type>(count)};

    auto t1 = high_resolution_clock::now();

    for (auto i = 0; i < (count >> 1); ++i) {
      table.emplace(data[i].first, data[i].second);
      inserted.insert(data[i].first);
    }

    auto t2 = high_resolution_clock::now();

    for (auto [op, idx] : ops) {

      switch(op) {

        case 1: // insert
        {
          auto result = table.emplace(data[idx].first, data[idx].second);
          [[maybe_unused]] auto it = inserted.insert(data[idx].first);
          (void)it;
          (void)result;
          break;
        }

        case 2: // retrieve
        {
          auto vPtr = table.find(data[idx].first);
          (void)vPtr;
          break;
        }

        case 3: // erase
        {
          auto result = table.erase(data[idx].first);
          (void)result;
          break;
        }

      }
    }

    auto t3 = high_resolution_clock::now();

    std::cout << "flat_hash_map: " << duration_cast<milliseconds>(t2 - t1).count()
              << ", " << duration_cast<milliseconds>(t3 - t2).count() << std::endl;
  }
}

TEST_CASE("timed insert, access and erase", "vich_hashing") {

  using namespace std::chrono;

  std::vector<std::tuple<milliseconds, milliseconds, milliseconds> > durations;

  int loop_count{10};
  for (int i = 0; i < loop_count; ++i) {

    using key_type = int;
    using mapped_type = int;

    int count{10000000};

    std::vector<std::pair<key_type, mapped_type> > data;
    data.reserve(count);

    for (auto i = 0; i < count; ++i) {
      //data.emplace_back(std::to_string(i), i + 1);
      //data.emplace_back(sole::uuid4().str(), i + 1);
      data.emplace_back(i, i + 1);
    }

    hashing::Vich<key_type, mapped_type> table{count};

    auto t1 = high_resolution_clock::now();

    for (auto i = 0; i < count; ++i) {
      table.insert(data[i].first, data[i].second);
    }

    auto t2 = high_resolution_clock::now();

    for (auto i = 0; i < count; ++i) {
      [[maybe_unused]]auto volatile rv = table.find(data[i].first);
    }

    auto t3 = high_resolution_clock::now();

    for (auto i = 0; i < count; ++i) {
      table.erase(data[i].first);
    }

    auto t4 = high_resolution_clock::now();

    auto d1 = duration_cast<milliseconds>(t2 - t1);
    auto d2 = duration_cast<milliseconds>(t3 - t2);
    auto d3 = duration_cast<milliseconds>(t4 - t3);

    std::cout << std::fixed << std::setprecision(3)
              << std::setw(12) << std::right << d1.count()
              << std::setw(12) << std::right << d2.count()
              << std::setw(12) << std::right << d3.count() << std::endl;

    durations.emplace_back(d1, d2, d3);
  }

  std::int64_t d1{}, d2{}, d3{};
  for (auto const& d : durations) {
    d1 += std::get<0>(d).count();
    d2 += std::get<1>(d).count();
    d3 += std::get<2>(d).count();
  }

  std::cout << '\n' << std::fixed << std::setprecision(3)
            << std::setw(12) << std::right
            << (static_cast<double>(d1) / loop_count)
            << std::setw(12) << std::right
            << (static_cast<double>(d2) / loop_count)
            << std::setw(12) << std::right
            << (static_cast<double>(d3) / loop_count) << std::endl;
}

TEST_CASE("timed insert, access and erase using unordered_map", "vich_hashing") {

  using namespace std::chrono;

  using key_type = int;
  using mapped_type = int;

  int count{10000000};

  std::vector<std::pair<key_type, mapped_type> > data;
  data.reserve(count);

  for (auto i = 0; i < count; ++i) {
    //data.emplace_back(std::to_string(i), i + 1);
    //data.emplace_back(sole::uuid4().str(), i + 1);
    data.emplace_back(i, i + 1);
  }

  using table_t = std::unordered_map<key_type, mapped_type>;

  table_t table{static_cast<table_t::size_type>(count)};

  auto t1 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    table.emplace(data[i].first, data[i].second);
  }

  auto t2 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    [[maybe_unused]]auto volatile rv = table.find(data[i].first);
  }

  auto t3 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    table.erase(data[i].first);
  }

  auto t4 = high_resolution_clock::now();

  std::cout << std::fixed << std::setprecision(3)
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t2 - t1).count()
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t3 - t2).count()
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t4 - t3).count()
            << std::endl;
}

TEST_CASE("timed insert, access and erase using flat_hash_map", "vich_hashing") {

  using namespace std::chrono;

  using key_type = int;
  using mapped_type = int;

  int count{10000000};

  std::vector<std::pair<key_type, mapped_type> > data;
  data.reserve(count);

  for (auto i = 0; i < count; ++i) {
    //data.emplace_back(std::to_string(i), i + 1);
    //data.emplace_back(sole::uuid4().str(), i + 1);
    data.emplace_back(i, i + 1);
  }

  using table_t = ska::flat_hash_map<key_type, mapped_type>;

  table_t table{static_cast<table_t::size_type>(count)};

  auto t1 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    table.emplace(data[i].first, data[i].second);
  }

  auto t2 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    [[maybe_unused]]auto volatile rv = table.find(data[i].first);
  }

  auto t3 = high_resolution_clock::now();

  for (auto i = 0; i < count; ++i) {
    table.erase(data[i].first);
  }

  auto t4 = high_resolution_clock::now();

  std::cout << std::fixed << std::setprecision(3)
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t2 - t1).count()
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t3 - t2).count()
            << std::setw(12) << std::right
            << duration_cast<milliseconds>(t4 - t3).count()
            << std::endl;
}

TEST_CASE("rehashing", "vich_hashing") {

  using key_type = std::string;
  using mapped_type = int;

  hashing::Vich<key_type, mapped_type> table{8};

  std::vector<std::pair<std::string, int> > data{

    {"Francis", 1}, {"Don", 2}, {"Leo", 3},
    {"Mike", 4}, {"Jeff", 5}, {"Dan", 6},
    {"Gary", 7}, {"Wen", 8}, {"Sharon", 9}

  };

  for (auto& [key, value] : data) {
    auto r = table.insert(key, std::move(value));
    REQUIRE(r == hashing::Vich<key_type, mapped_type>::Result::success);
  }

  SECTION("Rehashing with not enough slots will fail") {

    REQUIRE_FALSE(table.rehash(6));
    REQUIRE_FALSE(table.rehash(6, 2));
    table.set_address_factor(0.75);
    REQUIRE_FALSE(table.rehash(6));

  }

  SECTION("Rehash with enough slots") {

    SECTION("Version one, only address region slot count specified") {

      REQUIRE(table.rehash(15l));

    }

    SECTION("Version two, address region slot count and address_factor "
            "specified") {

      REQUIRE(table.rehash(15l, 3l));

    }

    SECTION("Version three, address region slot count and cellar slot count "
            "specified") {

      REQUIRE(table.rehash(15l, table.address_factor()));

    }
  }
}
